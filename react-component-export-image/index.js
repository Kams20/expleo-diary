import html2canvas from 'html2canvas';
import JsPDF from 'jspdf';
import ReactDOM from 'react-dom';

const fileType = {
    PNG: 'image/png',
    JPEG: 'image/jpeg',
    PDF: 'application/pdf'
};
/**
 * @param  {string} uri
 * @param  {string} filename
 */
const saveAs = (uri, filename) => {
    const link = document.createElement('a');

    if (typeof link.download === 'string') {
        link.href = uri;
        link.download = filename;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    } else {
        window.open(uri);
    }
};

/**
 * @param  {React.RefObject} node
 * @param  {string} fileName
 * @param  {string} backgroundColor
 * @param  {string} type
 * @param  {object} options=null
 */
const exportComponent = (node, fileName, backgroundColor, type, options) => {
    const element = ReactDOM.findDOMNode(node.current);
    return html2canvas(element, {
        backgroundColor: backgroundColor,
        scrollY: -window.scrollY,
        useCORS: true,
        ...options
    }).then(canvas => {
		 const pageWidth = doc.internal.pageSize.getWidth();
         const pageHeight = doc.internal.pageSize.getHeight();
		 const widthRatio = pageWidth / canvas.width;
         const heightRatio = pageHeight / canvas.height;
         const ratio = widthRatio > heightRatio ? heightRatio : widthRatio;

         const canvasWidth = canvas.width * ratio;
         const canvasHeight = canvas.height * ratio;

         const marginX = (pageWidth - canvasWidth) / 2;
         const marginY = (pageHeight - canvasHeight) / 2;
		 
        if (type === fileType.PDF) {
            const pdf = canvas.width > canvas.height
                ? new JsPDF('l', 'mm', [canvas.width, canvas.height])
                : new JsPDF('p', 'mm', [canvas.height, canvas.width]);
            pdf.addImage(canvas.toDataURL(fileType.PNG, 1.0), 'PNG', marginX, marginY, canvasWidth, canvasHeight);
            pdf.save(fileName);
        } else {
            saveAs(canvas.toDataURL(type, 1.0), fileName);
        }
    });
};
/**
 * @param  {React.RefObject} node
 * @param  {string} fileName='projectView.png'
 * @param  {string} backgroundColor=null
 * @param  {string} type=fileType.PNG
 * @param  {object} options=null
 */
const exportComponentAsPNG = (node, fileName = 'projectView.png', backgroundColor = null, type = fileType.PNG, options = null) => {
    return exportComponent(node, fileName, backgroundColor, type, options);
};
/**
 * @param  {React.RefObject} node
 * @param  {string} fileName='projectView.jpeg'
 * @param  {string} backgroundColor=null
 * @param  {string} type=fileType.JPEG
 * @param  {object} options=null
 */
const exportComponentAsJPEG = (node, fileName = 'projectView.jpeg', backgroundColor = null, type = fileType.JPEG, options = null) => {
    return exportComponent(node, fileName, backgroundColor, type, options);
};
/**
 * @param  {React.RefObject} node
 * @param  {string} fileName='projectView.pdf'
 * @param  {string} backgroundColor=null
 * @param  {string} type=fileType.PDF
 * @param  {object} options=null
 */
const exportComponentAsPDF = (node, fileName = 'projectView.pdf', backgroundColor = null, type = fileType.PDF, options = null) => {
    return exportComponent(node, fileName, backgroundColor, type, options);
};

export { exportComponentAsJPEG, exportComponentAsPDF, exportComponentAsPNG };
